<?php

class Storage
{
    protected static $instance;

    public static function instance()
    {
        if(static::$instance === null) {
            static::$instance = new Static();
        }

        return static::$instance;
    }

    protected function __construct()
    {
        session_start();
    }

    protected function __clone()
    {
    }

    protected function __wakeup()
    {
    }

    public function save($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public function load($key)
    {
        if (!isset($_SESSION[$key])) {
            return null;
        }
        return $_SESSION[$key];
    }

    public function drop($key)
    {
        unset($_SESSION[$key]);
    }

}

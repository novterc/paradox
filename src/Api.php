<?php

class Api
{

    public function createGame()
    {
        $game = Game::create();
        $this->responseSuccess([
            'id' => $game->id,
        ]);
    }

    public function getGame($id)
    {
        $game = $this->findByGameOrFail($id);
        $this->responseSuccess($game->getData());
    }

    public function setSelectDoor($id, $doorId)
    {
        $game = $this->findByGameOrFail($id);

        $selectDoor = intval($doorId);
        if($selectDoor < 1 || $selectDoor > Game::$maxNumberDoor) {
            $this->responseFail('invalid data');
        }

        $game->selectDoor = $selectDoor;
        $openDoor = $game->getOpenDoor();
        $game->save();

        $this->responseSuccess([
            'openDoor' => $openDoor,
        ]);
    }

    public function setChangeDoor($id)
    {
        $game = $this->findByGameOrFail($id);
        $game->setChangeDoor();
        $game->save();
        $this->responseSuccess();
    }

    public function getResult($id)
    {
        $game = $this->findByGameOrFail($id);
        $this->responseSuccess([
            'vin' => $game->getResult(),
        ]);
    }

    public function closeGame($id)
    {
        $game = $this->findByGameOrFail($id);
        $game->destroy();
        $this->responseSuccess();
    }

    protected function findByGameOrFail($id)
    {
        $game = Game::findById($id);
        if (empty($game)) {
            $this->responseFail('game by this id not exist');
        }

        return $game;
    }

    /////////////////////////////////////////////////////////////////////////

    protected function responseSuccess($data = [])
    {
        $data['status'] = true;

        $this->response($data);
    }

    protected function responseFail($reason)
    {
        $data['status'] = false;
        $data['reason'] = $reason;

        $this->response($data);
    }

    protected function response($data)
    {
        echo json_encode($data);
        die;
    }
}

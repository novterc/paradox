<?php

class Game
{
    protected $storage;
    public static $maxNumberDoor = 3;
    public $id;
    public $selectDoor;
    public $vinDoor;
    public $openDoor;
    public $isChageDoor;
    public $result;

    protected function __construct($data = [])
    {
        $this->storage = Storage::instance();
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public static function create()
    {
        $game = new self();
        $game->id = uniqid();
        $game->vinDoor = mt_rand(1, self::$maxNumberDoor);
        $game->save();

        return $game;
    }

    public static function findById($id)
    {
        $data = Storage::instance()->load($id);
        if(empty($data)) {
            return null;
        } else {
            return new self($data);
        }
    }

    public function getData()
    {
        $data = get_object_vars($this);
        unset($data['storage']);
        return $data;
    }

    public function save()
    {
        $this->storage->save($this->id, $this->getData());
    }

    public function getOpenDoor()
    {
        return $this->openDoor = $this->callOpenDoor();
    }

    public function setChangeDoor()
    {
        $this->isChageDoor = true;
    }

    public function getResult()
    {
        if($this->isChageDoor) {
            return ($this->selectDoor !== $this->vinDoor);
        } else {
            return ($this->selectDoor === $this->vinDoor);
        }
    }

    public function destroy()
    {
        $this->storage->drop($this->id);
    }

    protected function callOpenDoor()
    {
        $door = mt_rand(1, 3);
        if($door === $this->vinDoor || $door === $this->selectDoor) {
            return $this->callOpenDoor();
        } else {
            return $door;
        }
    }

}

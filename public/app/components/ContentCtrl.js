angular.module('myApp')
    .controller('ContentCtrl', ['$scope', 'Api',
        function ($scope, Api) {

            $scope.change = {
                total: 0,
                vin: 0,
                fail: 0,
            };
            $scope.nochange = {
                total: 0,
                vin: 0,
                fail: 0,
            };

            var limitGameChange = 0;
            var limitGameNoChange = 0;

            $scope.gameChange = function () {
                limitGameChange = 100;
                gameChange();
            };

            $scope.gameNoChange = function () {
                limitGameNoChange = 100;
                gameNoChange();
            };

            function gameChange() {
                Api.request('createGame').then(function (response) {
                    var id = response.id;
                    Api.request('setSelectDoor', id, 1).then(function () {
                        Api.request('setChangeDoor', id).then(function () {
                            Api.request('getResult', id).then(function (response) {
                                Api.request('closeGame', id)
                                $scope.$apply(function(){
                                    $scope.change.total++;
                                    if (response.vin) {
                                        $scope.change.vin++;
                                    } else {
                                        $scope.change.fail++;
                                    }
                                });
                                limitGameChange--;
                                if( limitGameChange > 0 ){
                                    gameChange();
                                }
                            });
                        });
                    });
                })
            }

            function gameNoChange() {
                Api.request('createGame').then(function (response) {
                    var id = response.id;
                    Api.request('setSelectDoor', id, 1).then(function () {
                        // Api.request('setChangeDoor', id).then(function () {
                            Api.request('getResult', id).then(function (response) {
                                $scope.$apply(function(){
                                    Api.request('closeGame', id)
                                    $scope.nochange.total++;
                                    if (response.vin) {
                                        $scope.nochange.vin++;
                                    } else {
                                        $scope.nochange.fail++;
                                    }
                                });
                                limitGameNoChange--;
                                if( limitGameNoChange > 0 ){
                                    gameNoChange();
                                }
                            });
                        // });
                    });
                })
            }


        }]);

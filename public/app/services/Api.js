angular.module('myApp')
    .service('Api', ['$http', function ($http) {

        var Api = this;
        Api.baseHost = '/api/';

        function implode( glue, pieces ) {
            return ( ( pieces instanceof Array ) ? pieces.join ( glue ) : pieces );
        }

        function getArrayArguments(arg) {
            var res = [];
            for(i=0; i<arg.length; i++) {
                res.push(arg[i]);
            }
            return res;
        }

        Api.request = function (method, v1, v2) {
            var url = Api.baseHost + implode('/', getArrayArguments(arguments));
            return new Promise(function (resolve, reject) {
                $http.get(url).then(function(response){
                    Api.handlerResponse(response, resolve, reject);
                }, function(){
                    if(reject == null){
                        reject = Api.handlerResponseFail;
                    }
                });
            });
        };

        Api.handlerResponseFail = function(reject)
        {
            console.log(['response fail', reject]);
        };

        Api.handlerResponse = function (response, resolve, reject) {
            if( response.status === 200 ) {
                if (response.data.status === true) {
                    resolve(response.data);
                    return true;
                }
            }

            reject(response);
        };

    }]);

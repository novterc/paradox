<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

define('BASEDIR', dirname(__DIR__));

$root = $_SERVER['REQUEST_URI'];
$rootPath = explode('?', $root)[0];
$rootArr = explode('/', $root);


if ($rootArr[1] === 'api' && !empty($rootArr[2])) {
    require BASEDIR . '/src/Api.php';
    require BASEDIR . '/src/Game.php';
    require BASEDIR . '/src/Storage.php';

    $api = new Api();

    $methodName = (string)$rootArr[2];
    $merhodAttrs = array_slice($rootArr, 3);
    if (method_exists($api, $methodName)) {
        call_user_func_array([$api, $methodName], $merhodAttrs);
    } else {
        die('method not exist');
    }

} else {
    require BASEDIR . '/template/main.html';
}
